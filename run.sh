#!/bin/bash

if [ $1 == "update" ] ; then
	sudo docker-compose run app yarn && yarn migration:run

fi

if [ $1 == "run" ]; then
	sudo docker-compose up -d
fi
