import * as path from 'path';
import * as Joi from '@hapi/joi';
import { ConfigModuleOptions } from '@nestjs/config/dist/interfaces';
import database from '@app/config/database.config';

const nodeEnv = process.env.NODE_ENV || 'development';

const configModuleOptions: ConfigModuleOptions = {
  isGlobal: true,
  envFilePath: path.join(__dirname, '..', '..', `.env.${nodeEnv}`),
  load: [database],
  validationSchema: Joi.object({
    NODE_ENV: Joi.string()
      .valid('development', 'staging', 'production', 'test')
      .default('development'),
    PORT: Joi.number().default(3000),
  }),
  validationOptions: {
    allowUnknowns: true,
    abortEarly: true,
  },
};

export default configModuleOptions;
